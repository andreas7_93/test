import React, {PureComponent} from 'react';
import styled from  'styled-components';
import Input from "../../Components/Form/Input";
import Link from "../../Components/Link";
import Button from "../../Components/Form/Button";
import Form from "../../Components/Form/Form";
import {data} from "../../Data/data";
import img from "../../assets/mainBG.jpg"

class RegistrationPage extends PureComponent {
    constructor(props){
        super(props);

        this.onEmailChange = this.onEmailChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.onRepeatPasswordChange = this.onRepeatPasswordChange.bind(this);
    }

    onEmailChange(event){
        this.props.setEmailText(event.target.value);
    }

    onPasswordChange(event){
        this.props.setPasswordText(event.target.value)
    }

    onRepeatPasswordChange(event){
        this.props.setRepeatPasswordText(event.target.value)
    }


    render() {
        const {email, password, repeatPassword} = this.props;
        return (
            <ContentWrapper>
                <Form action={'#'} width={'400px'}>
                    <Title>Регистрация</Title>
                    <Input label={'Логин:'}
                           name={'email'}
                           value={email}
                           placeholder={'Ваш логин'}
                           margin={'0 0 18px 0'}
                           onChange={this.onEmailChange}/>
                    <Input label={'Пароль:'}
                           type={'password'}
                           name={'password'}
                           placeholder={'Ваш пароль'}
                           margin={'0 0 17px 0'}
                           value={password}
                           onChange={this.onPasswordChange}/>
                    <Input label={'Повтор пароля:'}
                           type={'password'}
                           name={'repeatPassword'}
                           placeholder={'Ваш пароль повторно'}
                           margin={'0 0 17px 0'}
                           labelWidth={'100px'}
                           value={repeatPassword}
                           onChange={this.onRepeatPasswordChange}/>
                    <Link href={'/#/'} title={'Логин'} textAlign={'left'} margin={'0 0 16px 0'}/>
                    <Button title={'Регистрация'}
                            type={'submit'}
                            onClick={(e)=>{
                                e.preventDefault();
                                let error = data.some((item)=>{
                                    return item.name === email
                                })
                                if(error) console.log('Такой логин уже используется!')
                                else {
                                    if (password === repeatPassword) {
                                        data.push({name: email, password: password});
                                        console.log('Регистрация прошла успешно');
                                    } else console.log('Пароли не совпадают');
                                }
                            }}/>
                </Form>
            </ContentWrapper>
        );
    }
}


export default RegistrationPage;

const ContentWrapper = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    background: url(${img}) no-repeat 50% 50%;`;

const Title = styled.div`
    color: #fff;
    font-weight: bold;
    font-family: 'Gotham Pro', 'Arial', sans-serif;
    text-transform: uppercase;
    margin-bottom: 23px;
    text-align: center;`;