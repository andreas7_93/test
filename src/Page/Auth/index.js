import React, {PureComponent} from 'react';
import styled from  'styled-components';
import Input from "../../Components/Form/Input";
import Link from "../../Components/Link";
import Button from "../../Components/Form/Button";
import Form from "../../Components/Form/Form";
import Popup from "../../Components/Popup";
import {data} from "../../Data/data";
import img from "../../assets/mainBG.jpg";



class AuthPage extends PureComponent {
    constructor(props) {
        super(props);
        this.onEmailChange = this.onEmailChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.state = {
            isOpenPopup: false,
            logIn: false,
        };
    }

    onEmailChange(event){
        this.props.setEmailText(event.target.value);
    }

    onPasswordChange(event){
        this.props.setPasswordText(event.target.value);
    }

    render() {
        const {email, password} = this.props;
        const {isOpenPopup, logIn} = this.state;
        return (
            <ContentWrapper>
                <Form action={'#'} width={'400px'}>
                    <Title>Логин</Title>
                    <Input label={'Логин:'}
                           name={'email'}
                           value={email}
                           placeholder={'Ваш логин'}
                           margin={'0 0 18px 0'}
                           onChange = {this.onEmailChange}/>
                    <Input label={'Пароль:'}
                           type={'password'}
                           name={'password'}
                           value={password}
                           placeholder={'Ваш пароль'}
                           margin={'0 0 17px 0'}
                           onChange={this.onPasswordChange}/>
                    <Link href={'/#/registration/'}
                          title={'Регистрация'}
                          textAlign={'left'}
                          margin={'0 0 16px 0'}/>

                    <Button title={'Войти'}
                            type={'submit'}
                            onClick={e=>{
                                e.preventDefault();
                                data.forEach((item)=>{
                                    item.name === email && item.password === password &&
                                    this.setState({logIn: true})
                                })
                                this.setState({isOpenPopup: true});
                            }}/>
                </Form>
                {
                    isOpenPopup &&
                    <Popup title={ logIn ? 'Успех' : 'Провал'} onClose={()=>this.setState({isOpenPopup: false})}>
                        { logIn ?  'Пользователь ' + email + ' успешно вошел в систему' : 'Неверная связка логина и пароля, попробуйе снова'}
                    </Popup>

                }

            </ContentWrapper>
        );
    }
}


export default AuthPage;

const ContentWrapper = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    background: url(${img}) no-repeat 50% 50%;
    @media (max-width: )`;

const Title = styled.div`
    color: #fff;
    font-weight: bold;
    font-family: 'Gotham Pro', 'Arial', sans-serif;
    text-transform: uppercase;
    margin-bottom: 23px;
    text-align: center;`;