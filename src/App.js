import React, {PureComponent} from 'react';
import {createStore} from "redux";
import {Provider} from 'react-redux';
import rootReducer from './store/reducers';
import {Route ,HashRouter} from "react-router-dom";
import AuthContainer from "./Page/Auth/AuthContainer";
import RegistrationPageContainer from "./Page/Registration/RegistrationPageContainer";

const store = createStore(rootReducer)


class App extends PureComponent {
    render() {
        return (
            <Provider store={store}>
                <HashRouter>
                    <Route exact path={"/"} component={AuthContainer}/>
                    <Route path={"/registration/"} component={RegistrationPageContainer} />
                </HashRouter>

            </Provider>
        );
    }
}

export default App;