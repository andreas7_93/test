import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';


class Link extends PureComponent {

    render() {

        const {title, href, width, textAlign, margin} = this.props;

        return (
            <LinkWrapper width={width} textAlign={textAlign} margin={margin}>
                <a href={href}>{title}</a>
            </LinkWrapper>
        );
    }
}

Link.propTypes = {
    title: PropTypes.string,
    href: PropTypes.string,
    width: PropTypes.string,
    textAlign: PropTypes.string,
    margin: PropTypes.string,
};

Link.defaultProps = {
    title: 'Название',
    href: '',
}

export default Link;

const LinkWrapper = styled.div`
    
    font-family: 'Gotham Pro','Arial', sans-serif;
    ${(props) => (props.width && 'width:' + props.width)};
    ${(props) => (props.textAlign && 'textAlign:' + props.textAlign)};
    ${(props) => (props.margin && 'margin:' + props.margin)};
    a{
        color: white;
        text-decoration: none;
        text-transform: none;
        border-bottom: 1px solid #fff;
        font-size: 12px;
        cursor: pointer;
     }
    `;