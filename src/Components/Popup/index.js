import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Close from "../../assets/icons/Close";


class Popup extends PureComponent{

    render() {

        const {title, children, onClose} = this.props;

        return (
            <PopupWrapper>
                <PopupBody>
                    <PopupClose onClick={onClose}><Close/></PopupClose>
                    {title && (<PopupTitle> {title} </PopupTitle>)}
                    <PopupContent padding={title}>
                        {children}
                    </PopupContent>
                </PopupBody>
            </PopupWrapper>
        );
    }
}



Popup.propTypes = {
    title: PropTypes.string,
    children: PropTypes.node,
    onClose: PropTypes.func,
};

Popup.defaultProps = {
    children: null,
}




export default Popup;

const PopupWrapper = styled.div`
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: rgba(0, 0, 0, 0.7);`;

const PopupBody = styled.div`
    position: relative;
    z-index: 1;
    width: 420px;
    background-color: #fff;
    box-sizing: border-box;
    padding: 19px 24px 25px 20px;
    border-radius: 10px;
    @media (max-width: 450px){
        width: 85%;
    }`;

const PopupClose = styled.div`
    position: absolute;
    top: 24px;
    right: 24px;
    width: 14px;
    height: 14px;
    cursor: pointer;
    svg{
        width: 100%;
        height: auto;}
    `;

const PopupTitle = styled.div`
    margin-bottom: 25px;
    padding-right: 16px;
    font-size: 18px;
    font-weight: bold;
    font-family: 'Gotham Pro', 'Arial', sans-serif;
    color: #60474D;`;

const PopupContent = styled.div`
    font-size: 12px;
    line-height: 15px;
    color: #000;
    padding: ${props => props.padding ? '0' : '0 16px 0 0'}`;