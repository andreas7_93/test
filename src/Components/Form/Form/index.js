import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';


class Form extends PureComponent {
    render() {
        const {children, action, width} = this.props;
        return (
            <FormWrapper action={action} width={width}>
                {children}
            </FormWrapper>

        );
    }
}

Form.propTypes = {
    action: PropTypes.string,
    width: PropTypes.string,
};

export default Form;

const FormWrapper = styled.form`
    ${(props) => (props.width && 'width:' + props.width)};
    @media (max-width: 450px){
        width: 90%;
    }
`;