import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';


class Input extends PureComponent {

    render() {

        const {label, placeholder, margin, minWidth, value, name, onChange, labelWidth, type} = this.props;

        return (
            <InputWrapper margin={margin} minWidth={minWidth}>
                <LabelText labelWidth={labelWidth}> {label}  </LabelText>
                <input
                    type={type}
                    placeholder={placeholder}
                    value={value}
                    onChange={onChange}
                    name={name}/>
            </InputWrapper>
        );
    }
}

Input.propTypes = {
    label : PropTypes.string,
    placeholder: PropTypes.string,
    margin: PropTypes.string,
    minWidth: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func,
    name: PropTypes.string,
    labelWidth: PropTypes.string,
    type: PropTypes.string,
};

Input.defaultProps = {
    label: 'Текст',
    placeholder: '',
    margin: '0 0 0 0',
    type: 'text',
};

export default Input;

const InputWrapper = styled.label`
    border: 1px solid rgba(0, 0, 0, 0.05);
    border-radius: 5px;
    box-sizing: border-box;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    padding: 13px 12px 12px 12px;
    font-size: 12px;
    font-family: 'Gotham Pro', 'Arial', sans-serif;
    line-height: 125%;
    color: rgba(0, 0, 0, 0.7);
    cursor: pointer;
    background-color: #fff;
    margin: ${(props) => (props.margin ? props.margin : '0' )};
    ${(props) => (props.minWidth && 'min-width:' + props.minWidth )};
    input{
        width: 100%;
        border: none;
        outline: none;
        &:placeholder{
            color: rgba(0, 0, 0, 0.5);
        }
    }
`;

const LabelText = styled.div`
    font-weight: bold;
    margin-right: 24px;
    min-width: ${(props) => (props.labelWidth ? props.labelWidth : 'auto')};`;