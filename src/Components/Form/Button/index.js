import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

class Button extends PureComponent {
    render() {

        const {type, title, onClick} = this.props;

        return (
            <ButtonWrapper type={type} value={title} onClick={onClick}/>

        );
    }
}

Button.propTypes = {
    title: PropTypes.string,
    type: PropTypes.string,
    onClick: PropTypes.func,
};

export default Button;

const ButtonWrapper = styled.input`
    width: 100%;
    background-color: transparent;
    text-align: center;
    box-sizing: border-box;
    padding: 13px 5px 12px 5px;
    border: 1px solid  #35AD97;
    border-radius: 5px;
    color: white;
    cursor: pointer;
    outline: none;
    `;